package com.example.pages.gitlabpages;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitlabPagesApplication {

	public static void main(String[] args) {
		SpringApplication.run(GitlabPagesApplication.class, args);
	}

}
